---
title: Documentation
excerpt: Browse the Lua API documentation of Solarus.
tags: [doc, docs, documentation, api, lua]
aliases:
  - /docs
  - /documentation
layout: doc-redirect
---
