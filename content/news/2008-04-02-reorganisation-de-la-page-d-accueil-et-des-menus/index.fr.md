---
date: '2008-04-02'
excerpt: "Bonsoir à tous ! Vous l'avez sans doute remarqué : la page d'accueil vient de subir un léger lifting. Le cadre A l'affiche a été..."
tags:
  - solarus
title: Réorganisation de la page d'accueil et des menus
---

Bonsoir à tous !

Vous l'avez sans doute remarqué : la page d'accueil vient de subir un léger lifting. Le cadre "A l'affiche" a été remplacé par un message de bienvenue qui explique aux nouveaux arrivants ce que l'on peut trouver sur le site, et en particulier nos jeux amateurs. :)

Pourquoi ce changement aujourd'hui ? Tout simplement afin de mettre en valeur nos créations, en particulier Zelda Solarus DX, car le site va retrouver l'objectif qui était le sien au départ : **parler de nos Zelda amateurs** car ils sont à la base de ce qui fait l'originalité du site :). Dans cette même idée, les menus de gauche ont été réorganisés pour mieux mettre en avant nos créations. Nos trois Zelda amateurs (Mystery of Solarus, Mystery of Solarus DX et Mercuris' Chest) sont ainsi mieux visibles, et on peut accéder directement à leurs principales rubriques, notamment les téléchargements.

Bien sûr, tout cela ne nous empêche pas de continuer à parler de l'actualité des Zelda officiels, à vous proposer des soluces détaillées et surtout des mangas traduits en français par nos soins ^\_^
