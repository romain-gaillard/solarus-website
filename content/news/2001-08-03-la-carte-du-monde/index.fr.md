---
date: '2001-08-03'
excerpt: 'Salut à tous ! Alors quoi de neuf dans le développement du jeu ? Commençons par une mauvaise nouvelle : Thomas vient de partir en vacances et je...'
tags:
  - solarus
title: La Carte du Monde
---

Salut à tous !

Alors quoi de neuf dans le développement du jeu ? Commençons par une mauvaise nouvelle : Thomas vient de partir en vacances et je ne pourrai pas compter sur ses chipsets avant le mois de septembre. Ce qui ne m'empêche pas de progresser seul... je suis loin d'être au chômage technique.

Le troisième donjon est totalement terminé (mis à part la Carte, la Boussole et un petit bug concernant le Trésor du niveau). Je travaille en ce moment surtout à ce qui se passe entre le troisième et le quatrième donjon. La Carte du Monde s'est ainsi largement étendue depuis la démo. Voici un petit cadeau que vous attendez tous : un screenshot du Monde actuel en miniature. Vous pouvez reconnaître le village central et tous les lieux de la [démo](http://www.zelda-solarus.com/demo.php3) :

![](monde-miniature.png)

Comme vous le voyez beaucoup d'arbres et autres obstacles ont laissé la place à de nouveaux endroits à visiter... Mais le Monde sera beaucoup plus vaste dans la version finale. D'ici là il y a encore beaucoup à faire !
