---
date: '2001-12-25'
excerpt: "Voici votre cadeau de noël : six screenshots du niveau 7 ! Vous l'avez compris, le niveau 7 est un donjon glacial. Composé de..."
tags:
  - solarus
title: Joyeux Noël !
---

Voici votre cadeau de Noël : six screenshots du niveau 7 !

![](solarus-ecran51.png)

![](solarus-ecran52.png)

![](solarus-ecran53.png)

![](solarus-ecran54.png)

![](solarus-ecran55.png)

![](solarus-ecran56.png)

Vous l'avez compris, le niveau 7 est un donjon glacial.

Composé de trois grands étages, ce donjon apporte quelques nouveautés au jeu. Les énigmes seront assez novatrices par rapport aux autres donjons. Certaines sont inspirées des derniers donjons de Zelda 3...

Le donjon est quasiment terminé : il ne reste plus que le mini-boss et le Boss à programmer. Nous vous tenons au courant !
