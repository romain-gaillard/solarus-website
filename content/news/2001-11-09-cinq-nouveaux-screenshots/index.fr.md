---
date: '2001-11-09'
excerpt: Salut à tous ! Comme promis voici les screenshots du sixième donjon ! Ce donjon est encore plus vaste, encore plus long et encore plus dur que les...
tags:
  - solarus
title: Cinq nouveaux screenshots !
---

Salut à tous !

Comme promis voici les screenshots du sixième donjon ! Ce donjon est encore plus vaste, encore plus long et encore plus dur que les autres. Une des nouveautés sera qu'il est beaucoup moins linéaire que les autres. Ce qui donnera lieu à de nouvelles énigmes, en particulier avec les planchers-interrupteurs... Regardez plutôt les screenshots :

![](solarus-ecran44.png)

![](solarus-ecran45.png)

![](solarus-ecran46.png)

![](solarus-ecran47.png)

![](solarus-ecran48.png)
