---
date: '2004-03-26'
excerpt: 'La liste des membres de l''équipe du projet Zelda : MC (Zelda "Advanced Project") vient d''être actualisée car elle n''était plus vraiment à jour....'
tags:
- solarus
title: 'Mise à jour de la page Equipe de Zelda : M.C.'
---

La liste des membres de l'équipe du projet Zelda : MC (Zelda "Advanced Project") vient d'être actualisée car elle n'était plus vraiment à jour. En particulier, deux nouveaux membres y ont été ajoutés : MultiSync XV17, qui s'occupe d'extraire les animations des monstres de Zelda 3, et Neovyze, le dessinateur des futurs artworks officiels du jeu.

[Equipe de développement de Zelda : M.C.](http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=team)
