---
date: '2009-12-17'
excerpt: Bonsoir à tous, le moment est venu ! ^_^ Ca y est, nous sommes le vendredi 18 décembre, alors sans plus attendre... Je suis heureux de vous...
tags:
  - solarus
title: Téléchargez la démo de Zelda Solarus DX
---

Bonsoir à tous, le moment est venu ! ^\_^
Ca y est, nous sommes le vendredi 18 décembre, alors sans plus attendre... Je suis heureux de vous annoncer que vous pouvez y jouer... maintenant !

- [Télécharger la démo (version française, Windows)](http://www.zelda-solarus.com/download-zsdxdemo_fr_win32)

- [Autres versions](http://www.zelda-solarus.com/jeu-zsdx-demo)

J'espère que la démo vous plaira, sachez en tout cas que j'ai essayé de ne pas dévoiler absolument tout à l'avance :P. Amusez-vous bien !
