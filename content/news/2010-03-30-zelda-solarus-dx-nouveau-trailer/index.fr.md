---
date: '2010-03-30'
excerpt: Bonjour à tous, Je vous propose aujourd'hui une nouvelle vidéo de Zelda Mystery of Solarus DX. Il s'agit d'un trailer qui montre diverses...
tags:
  - solarus
title: 'Zelda Solarus DX : nouveau trailer !'
---

Bonjour à tous,

Je vous propose aujourd'hui une nouvelle vidéo de Zelda Mystery of Solarus DX. Il s'agit d'un trailer qui montre diverses séquences du jeu, dont quelques éléments inédits comme le deuxième donjon, l'arc, les escaliers?
Il faut noter que la musique de ce trailer est elle aussi inédite, elle a été créée par notre compositeur George. :)

Enjoy ! ^\_^

- [Voir le trailer](http://www.youtube.com/watch?v=HUMcFAdBJ_g)

- [Toutes les vidéos](http://www.zelda-solarus.com/jeu-zsdx-videos)
