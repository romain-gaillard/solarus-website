---
date: '2009-07-31'
excerpt: "Bonjour à tous, Aujourd'hui nous vous dévoilons 6 nouveaux artworks de Zelda : Mystery of Solarus."
tags:
  - solarus
title: 'Zelda Solarus DX : 6 nouveaux artworks de Neovyse'
---

Bonjour à tous,

Aujourd'hui nous vous dévoilons 6 nouveaux artworks de [Zelda : Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx). Réalisés par Neovyse, ils représentent les personnages principaux du jeu ainsi que l'Amulette du Solarus, un objet très important pour l'histoire :).

[![Zelda](http://www.zelda-solarus.com/images/zsdx/artworks/zelda_small.jpg)](http://www.zelda-solarus.com/images/zsdx/artworks/zelda_big.jpg)

[![Sahasrahla](http://www.zelda-solarus.com/images/zsdx/artworks/sahasrahla_small.jpg)](http://www.zelda-solarus.com/images/zsdx/artworks/sahasrahla_big.jpg)

[![Tom](http://www.zelda-solarus.com/images/zsdx/artworks/tom_small.jpg)](http://www.zelda-solarus.com/images/zsdx/artworks/tom_big.jpg)

[![Billy le Téméraire](http://www.zelda-solarus.com/images/zsdx/artworks/billy_small.jpg)](http://www.zelda-solarus.com/images/zsdx/artworks/billy_big.jpg)

[![Agahnim](http://www.zelda-solarus.com/images/zsdx/artworks/agahnim_small.jpg)](http://www.zelda-solarus.com/images/zsdx/artworks/agahnim_big.jpg)

[![L'Amulette du Solarus](http://www.zelda-solarus.com/images/zsdx/artworks/amulette_small.jpg)](http://www.zelda-solarus.com/images/zsdx/artworks/amulette_big.jpg)

Link a également été retravaillé :

[![Link](http://www.zelda-solarus.com/images/zsdx/artworks/link_small.jpg)](http://www.zelda-solarus.com/images/zsdx/artworks/link_big.jpg)

Félicitons Neovyse pour ce travail :P
