---
date: '2012-11-05'
excerpt: 'For you, gamers: ZSDX translations and ports A new version of Zelda Mystery of Solarus DX (1.5.2) should be available soon. I hope it will include...'
tags:
  - solarus
title: Work in progress
---

### For you, gamers: ZSDX translations and ports

A new version of [Zelda Mystery of Solarus DX](http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/ 'Zelda Mystery of Solarus DX') (1.5.2) should be available soon. I hope it will include the following new stuff:

- German translation: Stella has just finished to translate the game and she is now testing it in German.
- Android version (thanks to Sam101). We plan to merge the Android specific code into the main repository this weekend!
- [Pandora](http://www.openpandora.org) version (thanks to the OpenPandora community).
- Fix a few bugs, the most important one being: blocks made only half moves sometimes.

Android and Pandora versions will now become officially supported and maintained. In particular, I have updated the engine to allow a ratio wider than 4:3. Yes, it means that Android and Pandora players can see more content in their game screen!

### For you, game developers: engine mid-term plans

Our games ZSDX and ZSXD are released with the 0.9 branch of the Solarus engine. In that branch (branch master on our git repository), some elements of a game are customizable (the maps, the tilesets, the enemies, the treasures) and other are hardcoded (the title screen, the savegame screen, the pause screens, the HUD).

Since January, I've been working on improving the engine (branch 1.0) by making everything customizable and scriptable. In short, the ultimate goal is to turn the Zelda Mystery of Solarus DX engine into a general Zelda-like engine. When it's done, you will be able to make your own games with the Solarus engine.

To reach this goal, I decided to rewrite completely the Lua API. The new API is much more powerful, cleaner and easier to use. It provides real Lua datatypes and functions for everything that exists in the engine: maps, non-playing characters, enemies, sprites, movements, 2D surfaces, savegames, menus, timers, etc. The title screen, the other menus, the pause screen and the HUD can be scripted now. This is necessary for you to make your own games!

Let's take an example of why it is easier to use. Let's say that in the script of a map, you want to give animation `"walking"` to the non-playing character called `"tom"`.

With Solarus 0.9:

```lua
sol.main.sprite_set_animation(sol.map.npc_get_sprite("tom"), "walking")
```

With Solarus 1.0:

```lua
tom:get_sprite():set_animation("walking")
```

(In Lua, `":"` is the notation for object-oriented calls.)
Note that when you are in the script of a map, all map entities that have a name are accessible in the Lua environment by their name. Here, the variable `tom` automagically refers to the map entity named `"tom"`, which is of type non-playing character and has a method `get_sprite()`. Actually, this magic is just an equivalent shortcut to:

```lua
self:get_entity("tom"):get_sprite():set_animation("walking")
```

self is the current map since we are in a map script.
A more important thing is that all Lua scripts now share the same Lua world. With Solarus 0.9, all enemies for examples had their own Lua world. There was no easy way to access an enemy from the script of another one (this is often necessary for bosses involving several enemies). Now, from an enemy script, you can make (self is the current enemy):

```lua
local other = self:get_map():get_entity("another_enemy")
other:f()
```

where `f()` may be a built-in method in the type enemy (such as `get_life()`) or, on the contrary, a custom method specific to that particular enemy.

Everything is more flexible in the new Solarus API. You can also access enemies (as well as most datatypes) like tables to associate custom data to them but I will talk about this another day :)
