---
date: '2018-01-15'
excerpt: Comme vous le savez, nous travaillons actuellement sur un projet de remake de Link's Awakening nommé A Link to the Dream. Ce projet, qui est...
tags:
- solarus
title: La boutique du village des Mouettes
---

Comme vous le savez, nous travaillons actuellement sur un projet de **remake de Link's Awakening nommé A Link to the Dream**. Ce projet, qui est réalisé en partenariat avec [Zeldaforce](https://www.zeldaforce.net), utilise les **graphismes de A Link to the Past** et est à un stade bien avancé. En effet, nous travaillons actuellement d'arrache-pied sur ce jeu qui s'annonce vraiment prometteur.

Ces derniers temps, nous avons avancé sur la fameuse boutique du village des Mouettes et son marchand maléfique. Voilà un petit aperçu du jeu en espérant que cela vous plaise.
![](DTWLq-BW4AAdFJ5-300x238.jpg)

Nous comptons poster régulièrement différentes captures d'écran afin de vous donner des nouvelles régulières du projet.

Enfin, sachez qu'en ce moment Christopho travaille aussi sur le projet au niveau du mapping. D'ailleurs, ce soir à **21h00**, vous aurez l'occasion de suivre son travail en direct sur **Youtube**. Pour suivre, rien de plus simple, il suffit de cliquer sur ce [lien](https://www.youtube.com/watch?v=9TAgC_GnZCc).

Sachez que vous aurez aussi la possibilité de suivre le stream directement sur la page d'accueil de Zelda Solarus.
