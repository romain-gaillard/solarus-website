---
date: '2017-12-22'
excerpt: Salut ! Vous souvenez-vous des boss que vous aviez affrontés dans Mystery of Solarus DX ? Il faut reconnaùtre que Christopho avait réussi...
tags:
- solarus
title: Des combats dantesques
---

Salut !

Vous souvenez-vous des boss que vous aviez affrontés dans **Mystery of Solarus DX**?

Il faut reconnaître que Christopho avait réussi son pari d'enfin proposer des monstres très variés et animés pour ses différents donjons avec une bonne dose de challenge.

Avec **Mercuris' Chest**, nous allons essayer de réitérer cet exploit voire plus, avec des boss offrant des combats épiques et dantesques même si rien n'est encore gravé dans le marbre.

Aujourdhui, nous vous proposons avant les fêtes de fin d'années un concept art de l'un d'entre eux, en vous souhaitant de la part de toute l'équipe un joyeux Noël par avance. ;)

![](MCconceptartboss.png)
