---
date: '2017-04-01'
excerpt: Salut à toutes et à tous ! L'incroyable s'est produit ! Aujourd'hui, nous sommes en mesure de vous proposer, et ce bien avant la date...
tags:
- solarus
title: Zelda Mercuris Chess est sorti
---

Salut à toutes et à tous !

![zsxd2_title@2x](2x-300x249.png)

L'incroyable s'est produit !

Aujourd'hui, nous sommes en mesure de vous proposer, et ce bien avant la date annoncée de 2023, la version finale de notre nouveau Zelda : Mercuris Chess ! C'est non sans une certaine émotion et une certaine fierté que nous vous offrons cette création à télécharger dès maintenant.

![zsxd2_box](zsxd2_box-300x214.jpg)

- **[Télécharger Zelda Mercuris Chess](http://www.zelda-solarus.com/downloads/zelda-xd2/win32/zelda-xd2-1.0.3-win32.zip), le jeu complet**

Cette version finale du jeu est le fruit de longues nuits de développement, de mapping, de café et de Schoko-bons. Nous espérons que vous profiterez bien de cette nouvelle épopée de Link en 2D qui vous fera traverser des contrées merveilleuses, parcourir des donjons retords et évoquer quelques souvenirs.

![artwork_link](artwork_link-300x258.png)

Beaucoup d'entre vous devaient se douter qu'une sortie était proche, étant donné le rythme des news depuis le début de l'année ! Dites-nous si c'était le cas !

![artwork_grump](artwork_grump-292x300.png)

N'hésitez pas à commenter vos réactions sur les forums, nous avons hâte de découvrir votre ressenti sur cette nouvelle aventure qui clôt des années de préparation et de passion.

Pour finir je tiens à rendre hommage à toute l'équipe qui a travaillé d'arrache-pied sur ce projet complètement dingue. La Solarus Team a de quoi être fière, avec des gens qui ont énormément de talent et une motivation inépuisable ! Merci pour les souvenirs de Game Jam, bravo pour avoir mené ce projet à bien !

À bientôt et bon jeu !
