---
date: '2017-04-04'
excerpt: XD2 ou non, le screenshot de la semaine continue concernant Zelda Mercuris Chest. Aujourd'hui on vous présente un travail en cours par...
tags:
- solarus
title: Au pays des morts-vivants
---

XD2 ou non, le screenshot de la semaine continue concernant Zelda Mercuris Ches**t**.

Aujourd'hui on vous présente un travail en cours par l'inévitable Newlink dans : le cimetière !

![stalkis](stalkis-300x225.png)

On ne sait pas encore très bien ce qu'il y aura comme quêtes à remplir dans ces lieux, mais ces squelettes inédits ne vous rendront pas la tâche facile !
