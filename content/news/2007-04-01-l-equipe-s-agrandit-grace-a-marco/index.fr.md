---
date: '2007-04-01'
excerpt: 'Bonjour à tous :lol: Comme vous avez dû le remarquer, les mises à jour se font de moins en moins fréquentes ces derniers temps. En effet,...'
tags:
  - solarus
title: L'équipe s'agrandit gr&acirc;ce à Marco
---

Bonjour à tous :lol:

Comme vous avez dû le remarquer, les mises à jour se font de moins en moins fréquentes ces derniers temps. En effet, l'équipe actuelle manque cruellement de disponibilité pour s'occuper du site :naze:

Nous allons donc remédier à cela grâce à Marco qui rejoint l'équipe. Son rôle sera de suivre l'actualité Zelda. Vous n'êtes pas sans savoir que Marco est un vieux briscard du forum. Alors vous pouvez compter sur lui pour augmenter sensiblement la fréquence des news.

J'attends donc de vous, cher visiteurs, que vous lui réserviez un accueil triomphal !

:linkbravo: :linkbravo: :linkbravo:
