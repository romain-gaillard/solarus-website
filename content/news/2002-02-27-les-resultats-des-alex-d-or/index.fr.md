---
date: '2002-02-27'
excerpt: Le site officiel des Alex d'or (www.alexdor.fr.st) vient de communiquer les résultats du concours. Netgamer et moi sommes fiers de vous présenter...
tags:
  - solarus
title: Les résultats des Alex d'or
---

Le site officiel des Alex d'or ([www.alexdor.fr.st](http://www.alexdor.fr.st)) vient de communiquer les résultats du concours.

Netgamer et moi sommes fiers de vous présenter cette récompense : il s'agit de l'award du donjon-making !

![](award-alexdor.jpg)

Ce résultat est à hauteur de nos espérances, sachant que ce n'était que la démo qui était jugée. Rendez-vous dans un an pour l'édition des Alex d'or 2002, où cette fois le jeu final sera nominé !
