---
date: '2009-09-26'
excerpt: "Voici une petite nouvelle pour vous tenir au courant de l'avancement de notre création Zelda: Mystery of Solarus."
tags:
  - solarus
title: 'Zelda Solarus DX : la démo avance'
---

Voici une petite nouvelle pour vous tenir au courant de l'avancement de notre création [Zelda: Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx).

La [démo](http://www.zelda-solarus.com/index.php?id=745) est proche de la fin du développement. Rappelons que cette démo ira jusqu'à la fin du premier donjon et sortira le 18 décembre. Au niveau de la programmation, je viens de terminer le boss du donjon ce matin :). Un boss entièrement inédit, dessiné par Newlink ^\_^. La séquence de fin a elle aussi été terminée ces derniers jours.

Il me reste encore principalement deux choses à faire : le mini-boss, dessiné par @PyroNet, et l'intro du jeu car je souhaite l'inclure dans la démo. Ensuite, commenceront les phases de test et de traduction.
À bientôt pour d'autres nouvelles, avec des captures d'écran ^^.
