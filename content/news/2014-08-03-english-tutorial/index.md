---
date: '2014-08-03'
excerpt: More and more people are interested in creating games with Solarus. However, there was a lack of tutorials to explain how to do it. There is a...
tags:
- solarus
title: English tutorial!
---

More and more people are interested in creating games with Solarus. However, there was a lack of tutorials to explain how to do it.

There is a [video tutorial](http://wiki.solarus-games.org/doku.php?id=fr:video_tutorial) in French, and a [written tutorial](http://wiki.solarus-games.org/doku.php?id=fr:tutorial:create_your_2d_game_with_solarus) also in French, but so far there was nothing in English. You guys were expecting it, here it is: the official English video tutorial!

- [Chapter 1: Set up your project](https://www.youtube.com/watch?v=T9mEFmRVlBQ&index=1&list=PLzJ4jb-Y0ufwPrBfO5AQMRfe2kLABQsF0)
- [Chapter 2: Your first map](http://youtu.be/LALfp7c6rto)

I uploaded chapter 1 today, and more chapters will come in the next few days. The French tutorial has 55 episodes for now (and is not over!) so there is a lot of work! To get the updates, you can follow me on Twitter ([@ChristophoZS](https://twitter.com/ChristophoZS)) or subscribe to the YouTube channel.

We hope that you will enjoy these videos and more importantly, that you will be able to create lots of great games now :)
