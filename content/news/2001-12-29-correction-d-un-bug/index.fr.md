---
date: '2001-12-29'
excerpt: On vient de nous signaler qu'il y avait un gros bug sur la page de téléchargements. En effet, lorsque vous vouliez télécharger la démo, les RTP...
tags:
- solarus
title: Correction d'un bug
---

On vient de nous signaler qu'il y avait un gros bug sur la page de téléchargements. En effet, lorsque vous vouliez télécharger la démo, les RTP ou les polices d'écran françaises, vous arriviez sur la page de téléchargement mais celle-ci se fermait tout de suite et renvoyait à la page d'accueil, au lieu de démarrer le téléchargement.

Ce bug, dû au PHP et au nouveau design du site, est désormais corrigé. Vous pouvez donc télécharger sans problème :

- [La démo](http://www.zelda-solarus.com/download.php3?name=demo) (1 Mo)
- [Les RTP](http://www.zelda-solarus.com/download.php3?name=RTP) (11 Mo)
- [Les polices d'écran françaises (avec les accents)](http://www.zelda-solarus.com/download.php3?name=polices_icone)

Pour plus de détails sur les téléchargements, rendez-vous sur la page réservée à la [demo](http://www.zelda-solarus.com/demo.php3).
