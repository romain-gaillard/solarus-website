---
date: '2003-01-25'
excerpt: Depuis ce matin, tout le réseau Internet est en plein dérangements à la suite d'une attaque la nuit dernière. Asterochat, qui permet en temps...
tags:
- solarus
title: Téléchargez mIRC si vous n'arrivez pas à venir sur le chat !
---

Depuis ce matin, tout le réseau Internet est en plein dérangements à la suite d'une attaque la nuit dernière. Asterochat, qui permet en temps normal d'accéder au [chat](http://connexion.asterochat.com/?id=), semble marcher difficilement. Si vous n'arrivez pas vous y connecter, téléchargez mIRC en [cliquant ici](http://webperso.easynet.fr/mirc/mirc603.exe). Le logiciel ne fait que 1.16 Mo donc les petites connexions n'auront pas de problèmes.

**Une fois mIRC installé, lancez-le : une boîte de dialogue "mIRC Options" apparaît. Sélectionnez "EpiKnet" dans la liste "IRC Network" et tapez un pseudo dans "Nickname". Cliquez sur OK, et vous pourrez ensuite venir sur le chat en tapant ceci : /join #zeldasolarus.**

**Le concours commencera finalement vers 21 h 30** car beaucoup d'entre vous risquez d'arriver en retard à cause de ce contretemps.

Si vous n'arrivez pas ou si vous ne savez pas comment vous connecter, envoyez-moi un e-mail : [christopho@zelda-solarus.net](http://www.zelda-solarus.com/christopho@zelda-solarus.net) et je vous répondrai rapidement.

Voilà, désolé pour cet incident, on espère que vous viendrez quand même nombreux pour participer au concours !
