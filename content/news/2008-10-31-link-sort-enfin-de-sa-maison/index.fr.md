---
date: '2008-10-31'
excerpt: Jusqu'à présent, toutes les captures d'écran de Zelda Solarus DX étaient faites à l'intérieur d'une maison, pour la simple raison que le décor...
tags:
- solarus
title: Link sort enfin de sa maison !
---

Jusqu'à présent, toutes les captures d'écran de Zelda Solarus DX étaient faites à l'intérieur d'une maison, pour la simple raison que le décor de l'intérieur des maisons était le seul qui existait jusqu'ici. C'était suffisant pour développer les différents éléments du moteur de jeu comme les coffres, les vases, les rubis, la boîte de dialogue ou encore le menu de pause :).

Mais depuis ces derniers jours, nous sommes passés à l'étape supérieure : la création des maps ! Nous sommes en train de découper les décors du monde extérieur de A Link to the Past afin de pouvoir les réassembler de manière à créer les maps. La base des décors est prête, c'est-à-dire les falaises, l'eau et les chemins. Avec l'aide de Mymy, nous avons ainsi commencé à reproduire une bonne partie des maps de Mystery of Solarus ^\_^. Cela dit, il reste encore quelques éléments de décor importants à prendre en compte, comme les arbres et les maisons. Ensuite, on pourra vous montrer des images de ce que ça donne. Patience donc :P.

Bref, le moteur commence à être de plus en plus complet et nous nous attaquons à la quête principale, autrement dit la création des maps comme le monde extérieur, et bientôt les grottes, les autres maisons et... les donjons ! Cela ne signifie pas que le moteur est terminé puisqu'il manque des gros morceaux comme le système de combats, l'ouverture des portes, les personnages non joueurs, la possibilité de scripter les maps et j'en passe ^^. Tout ceci serait développé au fur et à mesure des besoins. Ce qui est sûr, c'est que vous serez tenus au courant ^\_^.
