---
date: '2003-01-01'
excerpt: Toute l'équipe de Zelda Solarus vous souhaite une excellente année 2003 ! Dans 4 heures pour les québecois... Nous vous souhaitons plein de...
tags:
  - solarus
title: BONNE ANNEE A TOUS !
---

![](bonneannee2003.gif)

Toute l'équipe de Zelda Solarus vous souhaite une excellente année 2003 ! Dans 4 heures pour les québecois... Nous vous souhaitons plein de bonheur et tous nos voeux en particulier en compagnie du zelda auquel vous allez jouer au mois de mai et pourquoi pas à Advanced Projet (version complète) à la fin de cette année...
