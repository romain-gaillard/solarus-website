---
date: '2003-07-26'
excerpt: Après l'effort, le réconfort ! Je pars en vacances demain matin et pour environ deux semaines. Mais pendant mon absence le site ne sera pas à...
tags:
- solarus
title: Vacances
---

Après l'effort, le réconfort ! Je pars en vacances demain matin et pour environ deux semaines. Mais pendant mon absence le site ne sera pas à l'abandon car un copain à moi est chargé de s'en occuper.

Merci de ne pas m'envoyer de mails pendant ces deux semaines car je ne pourrai pas y répondre avant mon retour.
