---
date: '2012-02-20'
excerpt: Bonjour à tous, Deux nouveaux épisodes de la soluce vidéo sont disponibles. La série va maintenant jusqu'à la fin du deuxième donjon.
tags:
  - solarus
title: 'Zelda Solarus DX : suite de la soluce'
---

Bonjour à tous,

Deux nouveaux épisodes de la soluce vidéo sont disponibles. La série va maintenant jusqu'à la fin du deuxième donjon.
Ces deux épisodes sont commentés par Mymy et moi-même :).

- [Épisode 3 : du niveau 1 au niveau 2](http://www.youtube.com/watch?v=PO-issFtjgY)

- [Épisode 4 : niveau 2 - Caverne de Roc](http://www.youtube.com/watch?v=PsEtixtmQ8s)

Même si vous n'êtes pas bloqués dans le jeu, ces vidéos ont l'air de vous plaire ^^. Nous allons bien sûr continuer la série, le tournage des prochains épisodes étant prévu pour cette semaine.
