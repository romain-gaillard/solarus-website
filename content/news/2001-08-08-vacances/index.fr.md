---
date: '2001-08-08'
excerpt: Dernière mise à jour avant les vacances ! En effet je pars cette nuit et pour dix jours. Je reviendrai donc vers le 19 août. Mais d'ici là vous...
tags:
  - solarus
title: Vacances !
---

Dernière mise à jour avant les vacances ! En effet je pars cette nuit et pour dix jours. Je reviendrai donc vers le 19 août. Mais d'ici là vous avez de quoi vous occuper : la démo, les 44 [screenshots](http://www.zelda-solarus.com/jeu-zs-scr) du jeu, toutes les [mises à jour](http://www.zelda-solarus.com/archives.php) archivées depuis le 4 Juillet et même avant, ainsi que le [Forum](http://forums.zelda-solarus.com). Si vous êtes bloqué(e) dans la démo, si vous avez trouvé des petites astuces ou des petits bugs, n'hésitez pas...

Dès mon retour les mises à jour reprendront leur rythme quasi-quotidien.

Dans la programmation du jeu, j'ai enfin réussi à virer un bug lié au trésor du donjon 3... J'ai passé une bonne partie de la journée d'hier à m'arracher les cheveux et je suis satisfait du résultat. Je ne vous en dis pas plus ! Mais ce que je peux vous dire, c'est que plus rien ne bloque la progression du développement du jeu entre le donjon 3 et le donjon 4. Donc je pourrai vous donner des nouvelles du jeu dès mon retour !

J'ai encore quelque chose à vous signaler : je viens de remarquer que les liens de l'image de gauche fonctionnaient mal et ouvraient toujours les pages dans une nouvelle fenêtre... Ce petit bug était lié au changement de design et à la disparition des frames. Maintenant tout marche correctement.

À bientôt :-)
