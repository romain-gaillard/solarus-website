---
date: '2018-01-22'
excerpt: Le semaine dernière, j'ai travaillé d'arrache-pied à l'élaboration du Temple du Masque. Pour ceux qui ne connaissent pas ce lieu mythique, il...
tags:
  - solarus
title: Le Temple du Masque
---

Le semaine dernière, j'ai travaillé d'arrache-pied à l'élaboration du **Temple du Masque**. Pour ceux qui ne connaissent pas ce lieu mythique, il s'agit du sixième donjon de Link's Awakening. A ce stade, je peux vous dire qu'il est terminé à 80% et promet d'être épique !

Voici deux images qui représentent des salles que vous reconnaitrez peut être.

![](masque1-300x240.png)

![](masque2-300x240.png)

N'hésitez pas à commenter et à dire ce que vous attendez le plus dans ce donjon ou même dans le projet !
En attendant, je vous dis à la semaine prochaine, pour de nouvelles informations croustillantes.
