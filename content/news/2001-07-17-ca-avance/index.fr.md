---
date: '2001-07-17'
excerpt: Salut à tous ! Une petite mise à jour pour vous informer de l'avancement du jeu. Je sais que vous êtes impatients mais il va falloir encore...
tags:
- solarus
title: Ca avance !!!
---

Salut à tous !

Une petite mise à jour pour vous informer de l'avancement du jeu. Je sais que vous êtes impatients mais il va falloir encore falloir attendre...

Comme prévu j'ai terminé le troisième donjon, et je vais m'attaquer aux nouveaux endroits de la Carte du Monde, à visiter pour arriver au donjon suivant. Thomas est en train de créer de nouvaux chipsets (décors), qu'il devrait m'envoyer un de ces jours. Grâce à son aide précieuse je vais pouvoir encore progresser sur la Carte du Monde.

Voilà, c'est tout pour aujourd'hui ! Très bientôt des nouvelles infos sur la programmation avec RPG Maker 2000.
