---
date: '2001-11-26'
excerpt: Salut à tous ! Malgré la métamorphose du site, le développement du jeu avance encore et toujours ! Il me reste encore une misérable dernière...
tags:
- solarus
title: Les news
---

Salut à tous !

Malgré la métamorphose du site, le développement du jeu avance encore et toujours ! Il me reste encore une misérable dernière grotte à programmer avant de m'attaquer au niveau 7.

Après cela il restera encore trois donjons à programmer, dont deux créés par Netgamer. Ce qui veut dire que le développement va pouvoir s'accélérer et que le jeu semble s'annoncer sans retard pour le mois de Mars !
