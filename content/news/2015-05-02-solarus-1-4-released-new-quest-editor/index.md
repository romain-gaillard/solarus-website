---
date: '2015-05-02'
excerpt: 'A new version of Solarus and Solarus Quest Editor is available: version 1.4! We have been working hard for the last few months to completely...'
tags:
  - solarus
title: Solarus 1.4 released, new quest editor!
---

A new version of Solarus and Solarus Quest Editor is available: version 1.4!

We have been working hard for the last few months to completely rewrite the quest editor. The new quest editor is now written in C++/Qt. It is faster, nicer and has a lot of new features!

![tileset-editor](tileset-editor-300x181.png)

- Dialogs editor!
- Strings editor!
- Quest properties editor!
- Options dialog!
- Ability to run the quest from the editor! (experimental)
- Multi-selection support in the tileset view!
- Improved quest tree!
- French translation!

And a lot more, see the full changelog below.

![dialog-editor](dialog-editor-300x181.png)

There might be some bugs we missed in the new editor since everything was rewritten from scratch. Please report any issue on the [bug tracker](https://github.com/christopho/solarus-quest-editor/issues/).

There are not a lot of changes in the engine (mostly bug fixes). The only real change is that fonts are now a resource like maps, tilesets, musics, sprites, etc. Your scripts may need slight adjustements to follow this change. Nothing dangerous, don't worry, but this is explained in the [migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide).

- [Download Solarus 1.4](http://www.solarus-games.org/download/ 'Download Solarus')
- [Solarus Quest Editor](http://www.solarus-games.org/engine/solarus-quest-editor/ 'Solarus Quest Editor')
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html 'LUA API documentation')
- [Migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide)

We hope you will enjoy the new quest editor! Check our [tutorials](http://www.solarus-games.org/development/tutorials/) if you want to learn how to create your own quest. We are also currently updating the website to reflect the new change and give up-to-date information and screenshots of the quest editor.

As always, our games ZSDX and ZSXD were also upgraded to give you up-to-date examples of games.

- [Download Zelda Mystery of Solarus DX 1.10](http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/ 'Download ZSDX')
- [Download Zelda Mystery of Solarus XD 1.10](http://www.solarus-games.org/games/zelda-mystery-of-solarus-xd/ 'Download ZSXD')

## Here is the full changelog. Changes in Solarus 1.4

New features:

- Solarus now compiles with C++11.
- Solarus can now be used as a library in other projects.
- Add a command-line flag -win-console=yes to see output on Windows (#550).
- Add unit tests.
- fonts.dat no longer exists. Fonts are a resource like others now (#611).
- Fonts are now in a "fonts" directory instead of "text".
- Maps: shop treasures have a new property "font".

Lua API changes that introduce incompatibilities:

- Text surfaces: the size must now be set at runtime instead of in fonts.dat.
- Text surfaces: the default font is now the first one in alphabetical order.

Lua API changes that do not introduce incompatibilities:

- sol.text_surface.create() now accepts a size parameter (default is 11).
- Add a function sol.main.get_os().
- Fix sprite:on_frame_changed() called twice on animation/direction change.

Bug fixes:

- Add mouse functions and events (experimental, more will come in Solarus 1.4).
- Add a method sprite:get_animation_set_id() (#552).
- Add a method sprite:has_animation() (#525).
- Add a method sprite:get_num_directions().
- Add a method hero:get_solid_ground_position() (#572).
- Add a method switch:get_sprite().
- Allow to **customize the sprite and sound of switches** (#553).
- Add a method enemy:get_treasure() (#501).

## Changes in Solarus Quest Editor 1.4

New features:

- Solarus Quest Editor was rewritten and is now in a separate repository.
- Dialogs editor (by Maxs).
- Strings editor (by Maxs).
- Quest properties (quest.dat) editor (by Maxs).
- French translation.
- Add an options dialogs to configure various settings (by Maxs).
- Add a toolbar.
- The quest can now be run from the editor (experimental).
- New keyboard shortcuts.
- Map editor: multiple tiles from the tileset can now be added at once.
- Map editor: improved the resizing of multiple entities.
- Map editor: show information about the entity under the cursor.
- Map editor: multiple tiles can be converted to/from dynamic tiles at once.
- Map editor: tiles whose pattern is missing are no longer removed.
- Tileset editor: undo/redo support.
- Tileset editor: multi-selection support.
- Tileset editor: showing the grid is now supported.
- Sprite editor: undo/redo support.
- Sprite editor: showing the grid is now supported.
- Sprite lists now show icons representing each sprite.
- Code editor: add a find text feature.
- Quest tree: organize the view in columns (file name, description, type).
- Quest tree: show resources whose file is missing on the filesystem.
- Quest tree: show files that look like resources but are not declared yet.
- The quest tree now automatically refreshes after changes in the filesystem.
- Resource selectors now show a hierarchical view of elements.
- A file to open can now be passed on the command line.
- Show an appropriate icon in tabs.
- Show an asterisk in the tab of files that are changed.
- The editor has an icon now.
- Add a menu item to open the official Solarus website.
- Add a menu item to open Solarus documentation.

Bug fixes:

- Fix encoding of non-ascii characters in project_db.dat with Windows.
- Fix Ctrl+S shortcut not always working.
- Fix entities restored to the front when undoing removing entities.
- Fix entities restored to the front when undoing changing their layer.
- Fix separators resizing that could allow illegal sizes.
- Fix jumpers size reset after undoing changing their direction.
- Fix walls being initially traversable by everything when they are created.

## Incompatibilities

## These improvements involve changes that introduce a slight incompatibility in the Lua API regarding fonts. Make a backup, and then see the [migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide) on the wiki to know how to upgrade. Changes in Zelda Mystery of Solarus DX 1.10

Bug fixes:

- Fix a minor tile issue in dungeon 8 entrance.
- Fix the feather dialog that could be skipped in some languages.
- Fix empty line in English dialog describing a bottle with water.
- Fix empty buttons in savegame menu when changing language (#90).
- Fix translation errors of two items in German (#98).
- Dungeon 5: fix a missing door on the minimap (#99).
- Dungeon 9: don't allow to skip the boss hint dialog (#93).

## Changes in Zelda Mystery of Solarus XD 1.10

Bug fixes:

- Fix fairy only restoring 1/4 heart in cursed cave (#40).
