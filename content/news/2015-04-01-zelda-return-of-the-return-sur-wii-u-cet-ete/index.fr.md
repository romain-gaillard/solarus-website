---
date: '2015-04-01'
excerpt: Un nouveau remake de Zelda serait-il en préparation ? C'est en tout cas ce que Satoru Iwata a laissé entendre lors du Nintendo Direct qui vient de...
tags:
  - solarus
title: Zelda Return of the Return sur Wii U cet été
---

Un nouveau remake de Zelda serait-il en préparation ? C'est en tout cas ce que Satoru Iwata a laissé entendre lors du Nintendo Direct qui vient de se terminer.  Si l'information est à prendre avec des pincettes, l'annonce a de quoi mettre l'eau à la bouche. En effet, ce serait pour la première fois un Zelda non-officiel que le géant nippon compterait mettre en lumière.

![Saturo Iwata lors du Nintendo Direct](nintendo_direct-300x175.png)

Il semblerait que Nintendo soit donc sensible aux créations des fans passionnés. Et c'est Zelda Return of the Hylian, le célèbre chef-d'uvre de Vincent Jouillat, qui devrait en effet devenir un jeu officiel. Réalisé par les studios de Solarus Software, c'est bel et bien sur Wii U, via l'e-store, que le jeu pourra être acheté.
![Logo Zelda Return of the Hylian](logo_roth-300x259.png)
L'incertitude demeure encore sur le titre définitif. Pour le moment, le titre provisoire reste inchangé en Zelda Return of the Hylian, mais selon les dernières fuites, le jeu pourrait à terme être renommé en Zelda Return of the Return afin de mieux marquer le côté remake et rendre hommage à l'original. C'est en tout cas ce que laisse penser la vidéo du Nintendo Direct :

{{< youtube DUmeln2kDTg >}}

Mais une chose est sûre : la date est quant à elle bien fixée, ce sera le 12 août sur le continent américain tout comme en Europe. Téléchargeable via l'e-store, le remake devrait coûter $9.99.![Zelda Return of the Hylian sur l'e-shop](roth_eshop1-300x168.png)
![Description de Zelda Return of the Return sur l'e-shop](roth_eshop_description-300x168.png)Du côté de Solarus Software comme de Vincent, c'est pour l'instant le silence radio. Les dirigeants n'ayant pas souhaité répondre à nos sollicitations, l'information n'est ni confirmée ni infirmée. Affaire à suivre
