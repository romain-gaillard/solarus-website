---
date: '2002-05-09'
excerpt: Nous avons remarqué que chez certaines personnes, les textes sont affichés avec des caractères bizarres, des N inversés... Si cela vous concerne,...
tags:
- solarus
title: Polices françaises...
---

Nous avons remarqué que chez certaines personnes, les textes sont affichés avec des caractères bizarres, des N inversés...

Si cela vous concerne, téléchargez vite les polices françaises pour corriger ce problème de typographie. Suivez les instructions dans le zip et vous pourrez lire correctement les textes dans ZS !

[ZS - Téléchargements](http://www.zelda-solarus.com/jeux.php3?jeu=zs&zone=download)
