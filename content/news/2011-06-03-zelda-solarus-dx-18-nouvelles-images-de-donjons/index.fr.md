---
date: '2011-06-03'
excerpt: "Après quelques semaines d'interruption liées au développement de notre mini-jeu parodique, la création des maps de Zelda : Mystery of Solarus DX est repartie de plus belle."
tags:
  - solarus
title: 'Zelda Solarus DX : 18 nouvelles images de donjons !'
---

Après quelques semaines d'interruption liées au développement de notre [mini-jeu parodique](http://www.zelda-solarus.com/jeu-zsxd), la création des maps de Zelda : Mystery of Solarus DX est repartie de plus belle. Plusieurs donjons sont en cours de développement par Metallizer et moi.

Pour être précis, voici une estimation approximative de l'avancement de chaque donjon :

- Niveau 1 (Donjon de la Forêt) : 100 %

- Niveau 2 (Caverne de Roc) : 98 %

- Niveau 3 (Antre de Maître Arbror) : 95 %

- Niveau 4 (Palais de Beaumont) : 0 %

- Niveau 5 (Ancien Château d'Hyrule) : 0 %

- Niveau 6 (Dédale d'Inferno) : 40 %

- Niveau 7 (Temple du Cristal) : 0 %

- Niveau 8 (Donjon des Pics Rocheux) : 80 %

- Niveau 9 (Temple des Souvenirs) : 1 %

Comme vous le voyez, on ne les fait pas dans l'ordre, on les fait plutôt selon l'envie du moment. Ce qui est intéressant, c'est que les donjons vont de plus en plus vite à se créer car le moteur de jeu devient très complet, de même que l'éditeur de maps qui nous permet d'être efficaces.

Nous sommes donc en mesure de publier 18 nouvelles captures d'écrans de quatre donjons différents. En voici 5 ci-dessous, les autres sont dans la [galerie d'images](http://www.zelda-solarus.com/jeu-zsdx-images). Les spécialistes reconnaîtront.

![](http://www.zelda-solarus.com/images/zsdx/dungeon_8_prickles.png)

![](http://www.zelda-solarus.com/images/zsdx/dungeon_6_crystals.png)

![](http://www.zelda-solarus.com/images/zsdx/dungeon_3_enemies.png)

![](http://www.zelda-solarus.com/images/zsdx/dungeon_2_miniboss.png)

![](http://www.zelda-solarus.com/images/zsdx/dungeon_2_big_chest.png)

- [Voir toutes les images](http://www.zelda-solarus.com/jeu-zsdx-images)
