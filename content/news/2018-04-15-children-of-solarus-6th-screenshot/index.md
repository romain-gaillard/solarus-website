---
date: '2018-04-15'
excerpt: Ahoy there! Here you have our 6th screenshot of the project Children of Solarus, which shows part of the castle map. From now on, we will post only 1...
tags:
  - solarus
title: Children of Solarus (6th screenshot)
---

Ahoy there! Here you have our 6th screenshot of the project Children of Solarus, which shows part of the castle map. From now on, we will post only 1 screenshot per month. Stay tuned for more incoming news![](6.-18-04-01-300x225.png)
