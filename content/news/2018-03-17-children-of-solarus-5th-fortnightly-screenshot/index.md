---
date: '2018-03-17'
excerpt: Ahoy there! In this colorful screenshot of the project "Children of Solarus" we show part of the castle, still unfinished and yet splendid....
tags:
- solarus
title: Children of Solarus (5th fortnightly screenshot)
---

Ahoy there! In this colorful screenshot of the project "Children of Solarus" we show part of the castle, still unfinished and yet splendid. :D![](5.-18-03-18-300x225.png)

Stay tuned for the next screenshot in two weeks!
