---
date: '2010-08-18'
excerpt: 'Several people have contacted me to know if I use a map editor for Zelda: Mystery of Solarus DX. The answer is yes, and its source code is available...'
tags:
  - solarus
title: The map editor
---

Several people have contacted me to know if I use a map editor for Zelda: Mystery of Solarus DX. The answer is yes, and its source code is available on the [Subversion repository](http://www.solarus-engine.org/source-code/). Even if there is no official release yet because some essential features are missing, I have just published a page that explains how to run [the development version of this editor](http://www.solarus-engine.org/quest-editor) to edit a quest.

![Quest editor](map_editor_5_mini.png 'Quest editor')
