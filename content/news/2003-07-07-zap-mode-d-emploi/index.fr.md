---
date: '2003-07-07'
excerpt: La démo de ZAP va sortir au mois de juillet. Mais j'ai pensé qu'en attendant vous aimeriez savoir comment on joue... Donc vous pouvez lire dès...
tags:
- solarus
title: 'ZAP : mode d''emploi'
---

La démo de ZAP va sortir au mois de juillet. Mais j'ai pensé qu'en attendant vous aimeriez savoir comment on joue... Donc vous pouvez lire dès maintenant le mode d'emploi pour connaître les commandes de jeu mais aussi de nouvelles infos et surtout de nouveaux screenshots car ce mode d'emploi est illustré !

[Mode d'emploi de la démo de Zelda : Advanced Project](http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=notice)
