---
date: '2002-06-30'
excerpt: ' Voici le bel award que nous a remis le site BlackSword à la suite de leur test de Zelda : Mystery of Solarus. Le jeu a reçu la note de 4/5 et je...'
tags:
- solarus
title: Award pour ZS
---

![](argentstar.jpg)

Voici le bel award que nous a remis le site BlackSword à la suite de leur test de Zelda : Mystery of Solarus. Le jeu a reçu la note de 4/5 et je dois dire en toute modestie que c'est une belle performance pour un jeu RM2K. Merci à l'équipe de BS pour ce test que vous pouvez retrouver [ici](http://membres.lycos.fr/sebeth/Test/ZeldaSolarus.php)
