---
date: '2018-03-21'
excerpt: Cette semaine, le projet A Link to the Dream a fortement avancé suite à la Game Jam qui a eu lieu. Du coup, voici une image qui a été élaborée...
tags:
- solarus
title: Les grottes (suite)
---

Cette semaine, le projet A Link to the Dream a fortement avancé suite à la Game Jam qui a eu lieu. Du coup, voici une image qui a été élaborée ce week end. J'espère que vous reconnaissez et que cela vous plait !

![](1-1-300x240.png)
