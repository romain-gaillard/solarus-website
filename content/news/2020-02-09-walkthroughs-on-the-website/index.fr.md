---
date: '2020-02-09'
excerpt: Le site propose désormais des solutions de jeux, et Mystery of Solarus DX est le premier servi.
tags:
  - website
thumbnail: cover.jpg
title: Solutions de jeux sur le site
---

Pour celles et ceux qui sont perdus et qui n'arrivent pas à finir les quêtes Solarus proposées sur ce site, nous sommes heureux de vous annoncer que le site permet désormais l'ajout de solutions de jeux.

La première solution à être en ligne est bien évidemment [celle pour _Mystery of Solarus DX_](/fr/walkthroughs/the-legend-of-zelda-mystery-of-solarus-dx). Renkineko a travaillé sur cette solution minutieuse et complète il y a quelques années, et l'a rédigée en anglais et français. Vous y trouverez des captures d'écran, les cartes des donjons, les emplacements des fragments de cœur et des instructions très précises pour finir le jeu à 100%. Alors un grand merci à lui pour avoir réalisé ce travail !
