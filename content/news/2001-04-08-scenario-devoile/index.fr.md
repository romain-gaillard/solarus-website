---
date: '2001-04-08'
excerpt: Ca faisait longtemps que nous ne vous donnions plus de nouvelles du jeu, et pourtant il est loin de tomber à l'abandon. Nous pensons avoir...
tags:
  - solarus
title: Scénario dévoilé
---

Ca faisait longtemps que nous ne vous donnions plus de nouvelles du jeu, et pourtant il est loin de tomber à l'abandon.

Nous pensons avoir maintenant le scénario quasi-définitif du jeu. Voici donc le début du scénario. La suite, vous la découvrirez en jouant !! L?histoire se passe peu de temps après la défaite de Ganon et de ses sbires dans The Legend of Zelda : a Link To The Past. L?action se déroule toujours au Monde de la Lumière, mais dans une autre partie du royaume : là où habitent désormais Link et Sahasrala.

La paix est revenue sur Hyrule, la Princesse Zelda et la famille royale vivent paisiblement. Mais un drame intervient : le Roi tombe étrangement malade et meurt peu de temps après. En plus du deuil qui s'en suit dans tout le Royaume, la mort du Roi a une lourde conséquence : le sceau des Septs Sages qui avait permis d'enfermer Ganon durant l'éternité s'est fragilisé. En effet, le Roi, maintenait le Sceau car il était le seul à pouvoir invoquer le pouvoir des sept Sages, en tant que descendant de ces derniers. Malheureusement, avec cette mort prématurée, le secret de ce Sceau disparaît en même temps que l'âme du pauvre Roi.

![](solarus-ecran9.jpg)

Link, craignant le retour du démoniaque Ganon, s'empresse alors de trouver l'Ancien, Sahasrala, dernier descendant des sept Sages après la famille royale, et qui l'avait tant aidé lors de sa première quête. L'Ancien lui confirme ses craintes : le Sceau étant fragilisé, Ganon risque de s'échapper de son Monde des Ténèbres.

Il supplie Link de prendre garde à la Triforce, qui est le seul élément permettant de rendre son pouvoir à Ganon. Sans la Triforce, Ganon ne peut conquérir le Monde de la Lumière. Se rendant compte de la terrible menace pesant sur son pays, Link confie la Triforce à la famille royale, composée de Zelda et de ses neuf frères et soeurs. En tant que descendants des sept Sages, ces derniers associent leurs pouvoirs pour séparer la Triforce en neuf fragments. Ces neuf fragments sont confiés à chacun des neuf frères et soeurs de Zelda, qui sont chargés de les disperser dans le Monde de la Lumière. Mais avant cela, ils prennent soin de créer une dernière protection, l'Amulette du Solarus, un talisman qui permet de réassembler les fragments de la Triforce pour restaurer son pouvoir. Ainsi la Triforce repose à nouveau en paix. Les habitants d'Hyrule croient pouvoir continuer leur paisible vie, sans se douter du danger énorme qui plane au-dessus de leur tête...

Scénario écrit par Christopho et Guillaume

Et c'est ici que le jeu commence. Vous découvrirez petit à petit la suite du scénario au cours du jeu !
