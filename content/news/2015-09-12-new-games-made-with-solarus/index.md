---
date: '2015-09-12'
excerpt: Solarus community had been very active for the last months. People started developing games and some are pretty far advanced. So we had to create a...
tags:
  - solarus
title: New games made with Solarus
---

Solarus community had been very active for the last months. People started developing games and some are pretty far advanced. So we had to create a [new page on Solarus website to feature community's games](http://www.solarus-games.org/games/community/).

Two games are well advanced in their development:

- _[Tunics!](http://tunics.legofarmen.se/)_: a rogue-like Zelda game. It means it's only dungeons, and they are randomly generated. If you die, you'll have to start again from the beginning.
- _[The Legend of Zelda: Book of Mudora](http://forum.solarus-games.org/index.php/topic,71.0.html)_: a more classic Zelda game.

Have fun !
