---
age: 'all'
controls: ['keyboard', 'gamepad']
developer: 'Solarus Team'
download: 'https://gitlab.com/solarus-games/games/zsxd/-/package_files/8119779/download'
excerpt: A short parodic game that makes fun of Zelda tropes, initially released as an April's fools.
genre: ['Action-RPG', 'Adventure']
id: zsxd
languages: ['en', 'fr', 'it']
license: ['GPL v3', 'CC-BY-SA 4.0', 'Proprietary (Fair use)']
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2011-04-01
latestUpdateDate: 2019-08-16
screenshots: ['screen1.png', 'screen2.png', 'screen3.png', 'screen4.png']
solarusVersion: 1.6.x
sourceCode: 'https://gitlab.com/solarus-games/games/zsxd'
thumbnail: 'thumbnail.png'
title: 'The Legend of Zelda: Mystery of Solarus XD'
version: 1.12.2
walkthroughs:
  - type: video
    url: 'https://www.youtube.com/watch?v=SNlWPqK8CSc&list=PL4170BC88D275AFDD'
---

### Presentation

_The Legend of Zelda: Mystery of Solarus XD_ is a parodic game that we released on April 1st, 2011. Though it’s a big April 1st joke, it’s a real, full game with two huge dungeons and 5-10 hours of playing.

It was developed in a very short period of time, thanks to coffee, beer and pizzas. Though the development has been rushed to ship on time, the game is complete, with lots of jokes, references and quirky NPCs.

![Link](artwork_link.png)

### Synopsis

As in many _Legend of Zelda_ games, your adventure begins by Link waking up. Though today, Link wakes up with no memory of what happened the previous night. It seems like he has drunk too much: he has lost his sword, his shield, and more important: he has lost track of Princess Zelda! Where is she? What happened to her? Was she present the previous night too?

And so begins your journey... or, to be more precise, your epic hangover.

![Zelda](artwork_zelda.png)
