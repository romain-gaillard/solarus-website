---
excerpt: Un remake à la façon SNES du classique culte de la Game Boy Link's Awakening.
---

### Présentation

_The Legend of Zelda: A Link to the Dream_ est un remake du classique culte de la Game Boy _The Legend of Zelda: Link's Awakening_, premièrement publié en 1993 et amélioré avec des couleurs en 1998 lors de sa ressortie en tant que version _DX_.

Ce remake fait avec Solarus utilise les graphismes d'_A Link to the Past_, améliorés avec de nombreux sprites et tiles originaux, dans le but de pouvoir représenter tous les personnages, ennemis et paysages du jeu. Même la musique a été refaite à la façon de celles de SNES, afin que l'illusion d'un jeu sorti sur Super Nintendo soit parfaite.

_A Link to the Dream_ est la lettre d'amour ultime pour la légendaire aventure de poche. Il n'a jamais été si agréable de parcourir l'île de Cocolint.

![The Owl](artwork_owl.png)

### Synopsis

Link naviguait tranquillement sur son voilier quand soudain une tempête se déclencha. Le tonnerre grondait et l'océan était déchaîné. Un éclair brisa le mât et détruit l'embarcation. Link perdit connaissance.

Il se retrouva sur la plage d'une île inconnue, toujours vivant mais blessé et évanoui. Une jeune fille nommée Marine le trouva et prit soin de lui jusqu'à son réveil. Elle lui expliqua qu'il se trouvait sur l'île de Cocolint, une île tropicale au milieu de l'océan. Link rencontra ensuite Tarkin, le père de Marine, qui avait trouvé le bouclier de Link sur la plage.

Qui sont ces personnages ? Mais où se trouve donc Link ? Pourquoi cette île semble à la fois si étrange et si familière ? Et surtout : que fait cet oeuf géant au sommet de la montagne ? Vous découvrirez la vérité en guidant Link au travers de cette quête épique et onirique.

![Marin](artwork_marin.png)
