---
title: Contact
excerpt: How to contact the people behind the Solarus game engine project.
tags: [contact]
aliases:
  - /contact
---

## Contact the community

Before contacting us, please see if your question is not already answered in the [Frequently Asked Questions](/about/faq) page!

The [Solarus Forums]({{< param "forumsURL" >}}) and the [Solarus Discord chat]({{< param "social.discord" >}}) are the recommended sources of discussions. You will usually get better answers there because the whole Solarus community can try to help. If you want to get more information, give a suggestion or propose a contribution, these are the right places.

## Contact us

However, you can also contact us directly at this email address if you have a specific question or if you need to contact us in private.

{{< email-obfuscator >}}
