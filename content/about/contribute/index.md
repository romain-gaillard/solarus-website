---
title: Contribute
excerpt: How to contribute to the Solarus game engine project.
tags: [source code, donation, donate, help, bug, translate, translation]
aliases:
  - /development/how-to-contribute
  - /en/development/how-to-contribute
---

## Engine

All the Solarus projects' source code are publicly available on Gitlab. The engine is made in C++. It is recommended to be proficient in this language and to master video game programming patterns before being able to contribute. The Quest Editor and the Launcher are made in C++ with Qt.

- [Solarus projects on Gitlab]({{< param "gitlab" >}})
- [C++ Engine Code documention](https://www.solarus-games.org/developer_doc/latest)

### Modify the source code

If you want to **add or modify the source code** of any project:

1. Clone the repository you want to contribute to.
2. Create your own branch and make your modifications in this branch.
3. Create a Merge Request.

### Inform of a bug you found

If you want to **inform of a bug**, or if you have a **feature request**:

1. Look if anyone hasn't already created an issue in the issue tracker of the project.
2. Create your issue, and be as precise as possible. Join screenshots if needed.

## Website

The website is made with [Hugo](https://gohugo.io). It uses a custom theme suited for our needs.

### Adding a game to the website

If your game development is complete, or enough advanced, you can add it to the Games pages of this website, which makes an inventory of games made with Solarus.

Create a _Merge Request_ when your branch is ready to be integrated, and we'll check if your game is eligible as well as the added files' correctness.

## Art

If you're feeling artistic, we need help in the pixel art department. Any tileset, font or sprite contributions are welcome! You may also want to improve the website or tools design?

- [Solarus graphic design elements on Gitlab](https://gitlab.com/solarus-games/solarus-design)
- [Solarus free resource pack on Gitlab](https://gitlab.com/solarus-games/solarus-free-resource-pack)

## Translations

### Game Editor and Game Launcher

The Quest Editor and the Launcher need to be translated in all the existing languages! Feel free to add your own language. Since they're built with Qt, you should Qt built-in translation manager named Qt Linguist.

- [Documentation about translating a Qt app](https://doc.qt.io/qt-5/qtlinguist-index.html).

### Games

The games may have new translations too. We have an in-house translator tool integrated into Solarus Quest Editor.

- [Documentation about translating a quest](https://www.solarus-games.org/doc/latest/translation.html).

### Website

We don't plan to translate the website in other languages than English and French at the moment.

## Donations

Solarus developers create this engine on their spare time. If you appreciate their work, your donations can help with the hosting fees and show that Solarus is appreciated.

- [Make a donation](/about/donate)
